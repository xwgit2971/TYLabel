Pod::Spec.new do |s|
  s.name = 'TYLabel'
  s.version = '0.0.1'
  s.platform = :ios, '8.0'
  s.license = { type: 'MIT', file: 'LICENSE' }
  s.summary = 'A drop-in replacement for UILabel that supports attributes, data detectors, links, and more. (基于TTTAttributedLabel)'
  s.homepage = 'https://gitlab.com/xwgit2971/TYLabel'
  s.author = { 'SunnyX' => '1031787148@qq.com' }
  s.source = { :git => 'git@gitlab.com:xwgit2971/TYLabel.git', :tag => s.version }
  s.source_files = 'TYLabel/*.{h,m}'
  s.framework = 'Foundation', 'UIKit'
  s.requires_arc = true
  s.dependency  'TTTAttributedLabel', '~> 2.0.0'
  s.dependency  'UIColor-TYAdditions'
end
