//
//  TYLabel.h
//  TYLabel
//
//  Created by 夏伟 on 2016/10/20.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import <TTTAttributedLabel/TTTAttributedLabel.h>

typedef void(^TYLabelTapBlock)(id aObj);

@interface TYLabel : TTTAttributedLabel

// 默认长按背景色: 浅灰色
- (void)addLongPressForCopy;
- (void)addLongPressForCopyWithBGColor:(UIColor *)color;

- (void)addTapBlock:(TYLabelTapBlock)block;
- (void)addDeleteBlock:(TYLabelTapBlock)block;

@end
