//
//  TYLabel.m
//  TYLabel
//
//  Created by 夏伟 on 2016/10/20.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import "TYLabel.h"
#import <UIColor-TYAdditions/UIColor+TYAdditions.h>

@interface TYLabel ()

@property (nonatomic, copy) TYLabelTapBlock tapBlock;
@property (nonatomic, copy) TYLabelTapBlock deleteBlock;
@property (nonatomic, assign) BOOL isSelectedForMenu;
@property (nonatomic, copy) UIColor *copyingColor;

@end

@implementation TYLabel

#pragma mark - Init
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup {
    _deleteBlock = nil;
    self.copyingColor = [UIColor add_colorWithRGBHexString:@"0xc0c1c2"];
}

#pragma mark - Override Methods
// UIMenuController
- (BOOL)canPerformAction:(SEL)action
              withSender:(nullable id)sender {
    BOOL canPerformAction = NO;
    if (action == @selector(copy:)) {
        canPerformAction = YES;
    } else if (action == @selector(delete:) && _deleteBlock) {
        canPerformAction = YES;
    }
    return canPerformAction;
}

#pragma mark - Tap
- (void)addTapBlock:(TYLabelTapBlock)block {
    _tapBlock = block;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [self addGestureRecognizer:tap];
}

- (void)addDeleteBlock:(TYLabelTapBlock)block {
    _deleteBlock = block;
}

- (void)handleTap:(UIGestureRecognizer*) recognizer {
    if (_tapBlock) {
        _tapBlock(self);
    }
}

#pragma mark - LongPress
- (void)addLongPressForCopy {
    _isSelectedForMenu = NO;
    UILongPressGestureRecognizer *press = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handlePress:)];
    [self addGestureRecognizer:press];
}

- (void)addLongPressForCopyWithBGColor:(UIColor *)color {
    self.copyingColor = color;
    [self addLongPressForCopy];
}

- (void)handlePress:(UIGestureRecognizer*)recognizer {
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        if (!_isSelectedForMenu) {
            _isSelectedForMenu = YES;
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(menuControllerWillHide:)
                                                         name:UIMenuControllerWillHideMenuNotification
                                                       object:nil];
            [self becomeFirstResponder];
            UIMenuController *menu = [UIMenuController sharedMenuController];
            [menu setTargetRect:self.frame inView:self.superview];
            [menu setMenuVisible:YES animated:YES];
            self.backgroundColor = self.copyingColor;
        }
    }
}

#pragma mark - Notifications
- (void)menuControllerWillHide:(NSNotification*)notification {
    if (_isSelectedForMenu) {
        _isSelectedForMenu = NO;
        self.backgroundColor = [UIColor clearColor];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIMenuControllerWillHideMenuNotification object:nil];
    }
}

#pragma mark - UIResponderStandardEditActions
- (void)copy:(nullable id)sender {
    [[UIPasteboard generalPasteboard] setString:self.text];
}

- (void)delete:(nullable id)sender {
    if (_deleteBlock) {
        _deleteBlock(self);
    }
}

@end
