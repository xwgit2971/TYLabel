//
//  ViewController.m
//  TYLabel
//
//  Created by 夏伟 on 2016/10/20.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import "ViewController.h"
#import "TYLabel.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet TYLabel *label;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.label addLongPressForCopy];
//    [self.label addLongPressForCopyWithBGColor:[UIColor blueColor]];
}

@end
